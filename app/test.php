<?php
require __DIR__ . '/vendor/autoload.php';
use Jcupitt\Vips;


$input = "files/3.png";
$waterMark = "files/4.png";
$output = "result2.png";
$background = "files/1.png";

// we can stream the main image
$image = Vips\Image::newFromFile($input, ['access' => 'sequential']);
$backgroudImage = Vips\Image::newFromFile($background, ['access' => 'sequential']);
$output_filename = $output;

// we'll read the watermark image many times, so we need random access for this
$watermark = Vips\Image::newFromFile($waterMark);

// the watermark image needs to have an alpha channel 
if(!$watermark->hasAlpha()) {
    echo("watermark image does not have an alpha channel\n");
    exit(1);
}

// remove the watermark alpha, scale it down, reattach ... you can skip this if
// you don't need to make the watermark semi-transparent
//$watermark_alpha = $watermark[$watermark->bands - 1];
//$watermark = $watermark->extract_band(0, ['n' => $watermark->bands - 1]);
//$watermark_alpha = $watermark_alpha->multiply(0.5);
//$watermark = $watermark->bandjoin($watermark_alpha);

// replicate the watermark image to make something big enough to cover the whole
// of image, then trim down to the exact size we need
$watermark = $watermark->replicate(
    1 + $image->width / $watermark->width,
    1 + $image->height / $watermark->height);
$watermark = $watermark->crop(0, 0, $image->width, $image->height);

// composite the watermark over the main imahe
$result = $image->composite2($watermark, 'in');
//$result = $backgroudImage->composite2($image,"over");
header("content-type: image/jpeg"); 
echo $result->pngsave_buffer();
